import unittest

import numpy

from d3m import metrics


class TestMetrics(unittest.TestCase):
    def _vectorize(self, targets):
        new_targets = []
        for target in targets:
            new_targets.append([target[0]] + [','.join(str(n) for n in target[1:5])] + target[5:])
        return new_targets

    def test_labels(self):
        predictions_list = [
            ['a'],
            ['b'],
            ['a'],
            ['b'],
            ['b'],
        ]
        ground_truth_list = [
            ['a'],
            ['b'],
            ['a'],
            ['b'],
            ['a'],
        ]

        self.assertEqual(metrics.precision(ground_truth_list, predictions_list, pos_label='a'), 1.0)
        self.assertEqual(metrics.precision(numpy.array(ground_truth_list), numpy.array(predictions_list), pos_label='a'), 1.0)
        self.assertAlmostEqual(metrics.precision(ground_truth_list, predictions_list, pos_label='b'), 0.6666666666666666)
        self.assertAlmostEqual(metrics.precision(numpy.array(ground_truth_list), numpy.array(predictions_list), pos_label='b'), 0.6666666666666666)

        # It does not work with list of lists.
        self.assertAlmostEqual(metrics.roc_auc(numpy.array(ground_truth_list), numpy.array(predictions_list), pos_label='a'), 0.8333333333333333)
        self.assertAlmostEqual(metrics.roc_auc(numpy.array(ground_truth_list), numpy.array(predictions_list), pos_label='b'), 0.8333333333333333)

    def test_object_detection_average_precision(self):
        predictions_list = [
            ['img_00285.png', 330, 463, 387, 505, 0.0739],
            ['img_00285.png', 420, 433, 451, 498, 0.0910],
            ['img_00285.png', 328, 465, 403, 540, 0.1008],
            ['img_00285.png', 480, 477, 508, 522, 0.1012],
            ['img_00285.png', 357, 460, 417, 537, 0.1058],
            ['img_00285.png', 356, 456, 391, 521, 0.0843],
            ['img_00225.png', 345, 460, 415, 547, 0.0539],
            ['img_00225.png', 381, 362, 455, 513, 0.0542],
            ['img_00225.png', 382, 366, 416, 422, 0.0559],
            ['img_00225.png', 730, 463, 763, 583, 0.0588],
        ]
        ground_truth_list = [
            ['img_00285.png', 480, 457, 515, 529],
            ['img_00285.png', 480, 457, 515, 529],
            ['img_00225.png', 522, 540, 576, 660],
            ['img_00225.png', 739, 460, 768, 545],
        ]

        self.assertAlmostEqual(metrics.object_detection_average_precision(ground_truth_list, predictions_list), 0.125)
        self.assertAlmostEqual(metrics.object_detection_average_precision(numpy.array(ground_truth_list, dtype=object), numpy.array(predictions_list, dtype=object)), 0.125)
        self.assertAlmostEqual(metrics.object_detection_average_precision(self._vectorize(ground_truth_list), self._vectorize(predictions_list)), 0.125)
        self.assertAlmostEqual(metrics.object_detection_average_precision(numpy.array(self._vectorize(ground_truth_list), dtype=object), numpy.array(self._vectorize(predictions_list), dtype=object)), 0.125)

        predictions_list = [
            ['img_00285.png', 330, 463, 387, 505],
            ['img_00285.png', 420, 433, 451, 498],
            ['img_00285.png', 328, 465, 403, 540],
            ['img_00285.png', 480, 477, 508, 522],
            ['img_00285.png', 357, 460, 417, 537],
            ['img_00285.png', 356, 456, 391, 521],
            ['img_00225.png', 345, 460, 415, 547],
            ['img_00225.png', 381, 362, 455, 513],
            ['img_00225.png', 382, 366, 416, 422],
            ['img_00225.png', 730, 463, 763, 583],
        ]
        ground_truth_list = [
            ['img_00285.png', 480, 457, 515, 529],
            ['img_00285.png', 480, 457, 515, 529],
            ['img_00225.png', 522, 540, 576, 660],
            ['img_00225.png', 739, 460, 768, 545],
        ]

        self.assertAlmostEqual(metrics.object_detection_average_precision(ground_truth_list, predictions_list), 0.0625)
        self.assertAlmostEqual(metrics.object_detection_average_precision(numpy.array(ground_truth_list, dtype=object), numpy.array(predictions_list, dtype=object)), 0.0625)
        self.assertAlmostEqual(metrics.object_detection_average_precision(self._vectorize(ground_truth_list), self._vectorize(predictions_list)), 0.0625)
        self.assertAlmostEqual(metrics.object_detection_average_precision(numpy.array(self._vectorize(ground_truth_list), dtype=object), numpy.array(self._vectorize(predictions_list), dtype=object)), 0.0625)


if __name__ == '__main__':
    unittest.main()
